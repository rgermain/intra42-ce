from django.db import models
from django.utils.translation import gettext as _
from common.manager import IntraCeQueryset


class Achievement(models.Model):
    """
        models for achievement
    """

    name = models.CharField(max_length=50)
    description = models.TextField()
    model = models.CharField(max_length=50)
    filters = models.TextField()
    threshold = models.PositiveIntegerField()

    LOOKUP_NAME = (
        ("NE", _("not equal")),
        ("EQ", _("equal")),
        ("LT", _("greater than")),
        ("LE", _("greater than equal")),
        ("GT", _("greater than")),
        ("GE", _("greater than equal")),
    )
    lookup_name = models.CharField(max_length=2, choices=LOOKUP_NAME)
    prefetch = models.TextField()
    select = models.TextField()
    objects = IntraCeQueryset.as_manager()

    def __str__(self):
        return self.name

    def __construct_filter(self, user):
        return self.filters.replace("{{user}}", user.id).split("/")

    def get_or_set(self, user):
        import operator

        try:
            classname = eval(self.model.capitalize())
            count = classname.objects.filter(*self.__construct_filter()).count()
            if getattr(operator, self.lookup_name[0].lower())(count, self.threshold):
                return SuiviAchievement.get_or_create(user=user, achievement=self)[0]
        except NameError:
            pass
        return None


class SuiviAchievement(models.Model):
    """
        models for achievement for user
    """

    date = models.TimeField(auto_now=True)
    user = models.ForeignKey("common.User", on_delete=models.CASCADE)
    achievement = models.ForeignKey("utility.Achievement", on_delete=models.CASCADE)
    popup = models.BooleanField(default=True)
    objects = IntraCeQueryset.as_manager()

    def __str__(self):
        return f"{self.__class__.__name__} of {self.user.__str__()}"

    @classmethod
    def select_std(cls):
        return ["user", "achievement"]
