from django.db import models
from django.utils.translation import gettext as _
from django.utils import timezone
from django.core.exceptions import ValidationError
from common.manager import IntraCeQueryset


class TimeSlot(models.Model):
    """
        models for timeslots in calendar
    """

    user = models.ForeignKey(
        "common.User", verbose_name=_(""), on_delete=models.CASCADE, null=False
    )
    start = models.DateTimeField(
        _("timeslot start"), auto_now=False, auto_now_add=False
    )
    end = models.DateTimeField(_("timeslot start"), auto_now=False, auto_now_add=False)
    objects = IntraCeQueryset.as_manager()

    def __str__(self):
        return f"Timeslot {self.user.__str__()} {self.start} {self.end}"

    @classmethod
    def select_std(cls):
        return ["user"]

    def delta(self):
        return self.end - self.start

    def clean(self, *args, **kargs):
        if self.start <= self.end or timezone.now() > self.start:
            raise ValidationError(_("Time slots not be upper than start"))
        super().clean()

    def save(self, *args, **kargs):
        super().save()
        # delete timeslots exists but end is less than now
        self.__class__.objects.filter(end__lt=timezone.now()).delete()


class PoolPoint(models.Model):
    """
        models for pool points
    """

    start = models.DateTimeField(auto_now=False, auto_now_add=False, null=False)
    end = models.DateTimeField(auto_now=False, auto_now_add=False, null=False)
    objects = IntraCeQueryset.as_manager()

    def __str__(self):
        return f"{self.start} - {self.end}"

    def hour(self):
        return self.end - self.start

