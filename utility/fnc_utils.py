from project.models import ProjectRegister
from django.utils.translation import gettext as _


def generate_notifications(obj):
    notif = []
    for value in obj:
        if isinstance(value, ProjectRegister):
            notif.append(
                {
                    "user": value.leader,
                    "text": _(f"Inivation au project {value.__str__()}"),
                    "date": value.start_or_end(),
                }
            )
    return notif
