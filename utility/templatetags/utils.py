from django import template

register = template.Library()


@register.simple_tag()
def ifelse(context, value1, value2=None):
    return value1 if context else value2


@register.filter
def color_project(value: int):
    return "bg-success" if value >= 75 else "bg-danger"
