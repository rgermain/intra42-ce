const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
  });

function toast(elment){
    if ($(elment).data('toast')) {
        let data = {
            title: $(elment).data('toast-title') || null,
            type: $(elment).data('toast-type') || 'success',
        };
        Toast.fire({
            type: data.type,
            title: data.title,
        });
    }
}

(function(){
    $('[data-copy]').each(function(){
        $(this).on('click', function(){
            let el = $('#' + $(this).data('copy'))[0]
            el.select();
            el.setSelectionRange(0, 99999);
            document.execCommand("copy");
            toast($(this));
        });
    });
})()

$( function() {
    let el = $( "#slider-range" )
    $(el).slider({
      range: true,
      min: $(el).data('min') || 0,
      max: $(el).data('max') || 500,
      values: [ $(el).data('min') || 0, $(el).data('max') || 500 ],
      slide: function( event, ui ) {
        let el = $($(this).data('write-range'));
        $(el).html(ui.values[ 0 ] + "xp - " + ui.values[ 1 ] + "xp");
        $(el).val(ui.values[ 0 ] + "" + ui.values[ 1 ]);
        $(el).change();
      }
    });
    let sel = $($("#slider-range" ).data('write-range'));
    $(sel).html($( "#slider-range" ).slider( "values", 0 ) +
      "xp - " + $( "#slider-range" ).slider( "values", 1 ) + "xp");
  }
);


$(function(){
    // notification
    $(document).ready(function(){
        $('.is_messages').each(function(){
            Toast.fire({
                type: $(this).data('type'),
                title: $(this).html(),
                timer: ($(this).data('type') == 'error') ? 0 : 3000,
            });
        });
    });

    // select2
    $('.select2').select2({
        allowClear: true,
    });

    $('.chartjs').each(function(){
        let obj = {
            fail: $(this).data('chart-fail'),
            success: $(this).data('chart-success'),
            notyet: $(this).data('chart-notyet'),
        }

        let chart = new Chart($(this)[0], {
            type: 'pie',
            data: {
                datasets: [{
                    data: [
                        obj.fail,
                        obj.success,
                        obj.notyet,
                    ],
                    backgroundColor: [
                        'red',
                        'green',
                        'grey',
                    ]
                }],
                labels: [
                    'fail',
                    'success',
                    'notyet',
                ],
            },
            options: {
                legend: {
                    display: true,
                },
                title: {
                    display: true,
                    text: 'Success Rate'
                }
            }
        });
    });
});
