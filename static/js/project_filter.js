$('#projectfilter').find('[data-filter]').each(function(){
    $(this).change(function(){
        let filter = "";
        let xp_min = -1;
        let xp_max = -1;

        $('#projectfilter').find('[data-filter]').each(function(){
            if ($(this).data('filter') == 'data-experience') {
                let el = $($(this).data('parent'));
                xp_min = $(el).slider( "values", 0 );
                xp_max = $(el).slider( "values", 1 );
            } else if ($(this).val() != 'null') {
                filter += `[${$(this).data('filter')}*=${$(this).val()}]`;
            }
        });

        let fnc_xp = function(val, min, max) {
            return ((min == -1 && max == -1) || (val >= min && val <= max));
        }

        $('[data-type="projectbox"]').each(function(){
            if (filter || !fnc_xp($(this).data('experience'), xp_min, xp_max)) {
                $(this).hide()
            } else {
                $(this).show()
            }
        });
        $(filter).each(function(){
            if (fnc_xp($(this).data('experience'), xp_min, xp_max)) {
                $(this).show();
            }
        });
        $('#boxlength').html($('[data-type="projectbox"]:visible').length);
    });
});