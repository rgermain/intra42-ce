from django.db import models
from django.utils.translation import gettext as _
from django.db.models import Min, Sum, F
from common.manager import IntraCeQueryset


class EvaluationFlags(models.Model):
    """
        evaluation flags like "Cheat", "Segfault" ...ect
    """

    name = models.CharField(_("evaluation flags"), max_length=50)
    socre = models.IntegerField(_(""), default=0)

    objects = IntraCeQueryset.as_manager()

    def __str__(self):
        return self.name


class Evaluation(models.Model):
    """
        models for evaluate a projects
    """

    evaluator = models.ForeignKey(
        "common.User",
        verbose_name=_("evaluator evaluation"),
        on_delete=models.SET_NULL,
        null=True,
    )
    project = models.ForeignKey(
        "project.ProjectRegister",
        verbose_name=_("projects evaluation"),
        on_delete=models.CASCADE,
        null=False,
    )
    comments = models.TextField(_("comments evaluation"))
    outstanding = models.BooleanField(_("outstanding projects"), default=False)
    date = models.DateTimeField(_("date evaluation"), auto_now=True, auto_now_add=False)
    flags = models.ManyToManyField(EvaluationFlags, verbose_name=_("flags evaluation"))
    scoring = models.ManyToManyField(
        "project.Scoring", verbose_name=_(""), through="EvaluationScoring"
    )
    final_score = models.IntegerField(_(""))
    is_validated = models.BooleanField(_("is validated"), default=False)

    objects = IntraCeQueryset.as_manager()

    def __str__(self):
        return f"{self.project.user.__str__()} evaluated by {self.evaluator.__str__()} on {self.project.project.__str__()}"

    @classmethod
    def select_std(cls):
        return ["evaluator", "project"]

    @classmethod
    def prefetch_std(cls):
        return ["flags", "scoring"]

    class Meta:
        ordering = ("date",)

    def save(self, *args, **kwargs):
        # get score from evaluation, if evluation have flags , get the minimum score from flags or sum all score
        if self.flags.count() > 0:
            self.final_score = self.flags.aggregate(min=Min("score"))["min"]
        else:
            self.final_score = self.flags.aggregate(min=Sum(F("calcul_score")))["min"]
        return super().save()


class EvaluationScoring(models.Model):
    evaluation = models.ForeignKey(
        Evaluation, verbose_name=_(""), on_delete=models.CASCADE, null=False
    )
    scoring = models.ForeignKey(
        "project.Scoring", verbose_name=_(""), on_delete=models.CASCADE, null=False
    )
    score = models.PositiveIntegerField(_(""))
    objects = IntraCeQueryset.as_manager()

    def __str__(self):
        return f"Scoring {self.evaluation.__str__()}"

    @classmethod
    def select_std(cls):
        return ["evaluation", "scoring"]

    def calcul_score(self):
        return self.score * self.scoring.coef


class FeedBackEvaluation(models.Model):
    evaluation = models.ForeignKey(
        Evaluation, verbose_name=_(""), on_delete=models.CASCADE, null=False
    )
    evaluator = models.ForeignKey(
        "common.User",
        verbose_name=_("evaluator evaluation"),
        on_delete=models.SET_NULL,
        null=True,
    )
    comments = models.TextField(_("comments feedback"))
    objects = IntraCeQueryset.as_manager()

    # score

    def __str__(self):
        return f"{self.__class__.__name__} {self.evaluation.__str__()}"

    @classmethod
    def select_std(cls):
        return ["evaluation", "evaluator"]
