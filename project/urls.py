from django.urls import path
from .views import (
    ProjectList,
    ProjectDetail,
    ProjectAdminListView,
    ProjectAdminCreateView,
    ProjectRegisterUpdateView,
)

app_name = "project"

urlpatterns = [
    path("", ProjectList.as_view(), name="project-list"),
    path(
        "<int:pk>", ProjectDetail.as_view(), {"register": False}, name="project-detail"
    ),
    path(
        "register/<int:pk>",
        ProjectDetail.as_view(template_name="project/projectregister_detail.html"),
        {"register": True},
        name="project-register-detail",
    ),
    path(
        "register/update/<int:pk>",
        ProjectRegisterUpdateView.as_view(),
        name="project-register-update",
    ),
    path("admin/create", ProjectAdminCreateView.as_view(), name="project-create"),
    path("admin/", ProjectAdminListView.as_view(), name="project-tools"),
]
