from django.views.generic import (
    ListView,
    DetailView,
    TemplateView,
    CreateView,
    UpdateView,
)
from common.mixins import IntraCeMixin
from .models import Project, ProjectRegister, Category, ProgLang
from django.db.models import Max, Q
from common.models import User
from .forms import ProjectForm, ProjectRegisterForm
from django.utils.translation import gettext as _
from django.contrib import messages
from django.http import HttpResponseRedirect


class ProjectList(IntraCeMixin, ListView):
    model = Project

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        visitor = User.objects.first()
        context["allcategory"] = Category.objects.all()
        context["allproglang"] = ProgLang.objects.all()
        query = (
            ProjectRegister.objects.filter(Q(leader=visitor) | Q(teams=visitor))
            .distinct()
            .values_list("project__pk")
        )
        context["projectregister_pk"] = [obj[0] for obj in query]
        return context


class ProjectDetail(IntraCeMixin, DetailView):
    model = Project
    register_template = "project/projectregister_detail.html"

    def access_fnc(self):
        obj = self.get_object()
        visitor = User.objects.first()
        if not obj:
            return False
        if (
            self.kwargs["register"]
            and not ProjectRegister.objects.filter(
                Q(leader=visitor) | Q(teams=visitor), project=obj,
            ).count()
        ):
            return False
        return True

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        visitor = User.objects.first()
        context["register"] = ProjectRegister.objects.filter(
            Q(leader=visitor) | Q(teams=visitor), project=self.get_object()
        ).distinct()
        context["extra"] = ProjectRegister.extra(visitor, self.get_object())
        context["is_register"] = self.kwargs["register"]
        context["form_project"] = ProjectRegisterForm(
            instance=context["register"].first(),
            data=self.request.POST or None,
            creator=visitor,
        )
        return context

    def post(self, request, *args, **kwargs):
        visitor = User.objects.first()
        type_data = ("register", "close", "retry", "finish", "edit")
        project = Project.objects.get(id=kwargs["pk"])
        if "type" in request.POST and request.POST["type"] in type_data:
            try:
                obj = None
                if request.POST["type"] == "edit":
                    form = ProjectRegisterForm(instance=project, data=request.POST)
                    if not form.is_valid():
                        raise ValueError(form.errors)
                    form.save()
                    obj = project
                elif request.POST["type"] == "register":
                    obj = ProjectRegister.register(visitor, project)
                elif request.POST["type"] == "close":
                    obj = ProjectRegister.close(visitor, project)
                elif request.POST["type"] == "retry":
                    obj = ProjectRegister.retry(visitor, project)
                elif request.POST["type"] == "finish":
                    obj = ProjectRegister.finish(visitor, project)
                return HttpResponseRedirect(obj.get_absolute_url())
            except Exception as error:
                messages.error(self.request, error)
        else:
            messages.error(self.request, _("Mauvaise donner envoyer"))
        return self.get(self.request, *args, **kwargs)


class ProjectRegisterUpdateView(IntraCeMixin, UpdateView):
    model = ProjectRegister
    form_class = ProjectRegisterForm

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        visitor = User.objects.first()
        obj = self.get_object()
        if visitor == obj.leader and obj.is_waiting():
            return super().post(request, *args, **kwargs)
        messages.error(self.request, _("Vous ne pouvez pas modifier ce project"))
        return HttpResponseRedirect(obj.get_absolute_url())


class ProjectAdminListView(IntraCeMixin, ListView):
    model = Project
    template_name = "project/project_admin_list.html"

    def get_queryset(self):
        visitor = User.objects.first() or self.request.user
        return super().get_queryset().filter(creators=visitor)


class ProjectAdminCreateView(IntraCeMixin, CreateView):
    model = Project
    form_class = ProjectForm
    template_name = "project/project_admin_form.html"

    def post(self, request, *args, **kwargs):
        form = ProjectForm(data=request.POST)
        if form.is_valid():
            from_scoring = None
        return super().post(request, *args, **kwargs)
