from django.db import models
from django.utils.translation import gettext as _
from common.manager import IntraCeQueryset


class Objective(models.Model):
    name = models.CharField(max_length=80)

    def __str__(self):
        return self.name


class Skill(models.Model):
    name = models.CharField(max_length=80)

    def __str__(self):
        return self.name


class ProgLang(models.Model):
    """
        models for programming languages
    """

    name = models.CharField(max_length=50, null=False, blank=False)
    objects = IntraCeQueryset.as_manager()

    def __str__(self):
        return self.name


class Category(models.Model):
    """
        models for projects category
    """

    name = models.CharField(max_length=50, null=False, blank=False)
    objects = IntraCeQueryset.as_manager()

    def __str__(self):
        return self.name


class TagsVideo(models.Model):
    """
        models for projects category
    """

    name = models.CharField(max_length=50, null=False, blank=False)
    objects = IntraCeQueryset.as_manager()

    def __str__(self):
        return self.name


class ProjectLink(models.Model):
    """
        models for video link in projects
    """

    link = models.URLField(max_length=200)
    name = models.CharField(max_length=50)
    description = models.TextField()
    tags = models.ManyToManyField(TagsVideo)
    objects = IntraCeQueryset.as_manager()

    def __str__(self):
        return self.name

    @classmethod
    def prefetch_std(cls):
        return ["tags"]


class ProjectVersion(models.Model):
    date = models.DateField(auto_now=False, auto_now_add=False)
    description = models.TextField()


class ProjectIssue(models.Model):
    """
        models for submit issue on projects
    """

    user = models.ForeignKey("common.User", on_delete=models.CASCADE, null=False)
    date = models.DateTimeField(auto_now=True, auto_now_add=False)
    message = models.TextField()
    objects = IntraCeQueryset.as_manager()

    def __str__(self):
        return f"Issue by {self.user.__str__()}"

    @classmethod
    def select_std(cls):
        return ["user"]
