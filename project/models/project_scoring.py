from django.db import models
from common.manager import IntraCeQueryset


class Scoring(models.Model):
    """
        models for create a notations on projects evaluations
    """

    project = models.ForeignKey(
        "project.Project", on_delete=models.CASCADE, null=False,
    )
    order = models.PositiveIntegerField()
    question = models.TextField()
    description = models.TextField()
    # coef for this questions have coefitian notes
    coef = models.PositiveSmallIntegerField()
    case = models.ForeignKey(
        "project.ScoringCase", on_delete=models.CASCADE, null=False,
    )
    objects = IntraCeQueryset.as_manager()

    def __str__(self):
        return f"{self.__class__.__name__} {self.project.__str__()}"

    class Meta:
        ordering = ("order",)
        unique_together = ("project", "order")

    @classmethod
    def select_std(cls):
        return ["case", "project"]


class ScoringCase(models.Model):
    """
        models for case display type
        3 slider have min 3, max 5 point
        and one boolean type (True , False)
    """

    SCORING_CASE = (
        ("BOOL", "Boolean"),
        ("SLIDE_3", "Slider_3"),
        ("SLIDE_4", "Slider_4"),
        ("SLIDE_5", "Slider_5"),
    )
    case = models.CharField(max_length=7, choices=SCORING_CASE)
    objects = IntraCeQueryset.as_manager()

    def __str__(self):
        return self.case[1]
