from django.db import models
from django.utils.translation import gettext as _
from common.manager import IntraCeQueryset
from django.utils import timezone
from django.db.models import Count, Max, Q
from django.urls import reverse


class ProjectRegister(models.Model):
    """
        models for project's user register
    """

    leader = models.ForeignKey(
        "common.User", on_delete=models.SET_NULL, related_name="leaders", null=True
    )
    teams = models.ManyToManyField("common.User", related_name="teams", blank=True)
    invitations = models.ManyToManyField(
        "common.User", related_name="invitations", blank=True
    )
    project = models.ForeignKey("project.Project", on_delete=models.CASCADE, null=False)

    """
        status qui gere le satus du project , 
        __WAIT = en attente de l'equipe ..ect
        __PROGRESS = en cours sur le project
        __FINISH = le project est clos , vous ne pouvez plus le modifier
    """
    __WAIT = "WAIT"
    __PROGRESS = "PROGRESS"
    __FINISH = "FINISH"
    __CORRECT = "CORRECTED"

    STATUS_CHOICES = (
        (__WAIT, _("En attente")),
        (__PROGRESS, _("En Cours")),
        (__FINISH, _("Terminer")),
    )
    status = models.CharField(
        max_length=8, choices=STATUS_CHOICES, default=__WAIT, blank=False
    )

    """
        date du debut et de fin du project
    """
    started = models.DateTimeField(auto_now_add=True, null=False)
    finished = models.DateTimeField(null=True, blank=True)

    git = models.URLField(max_length=200, blank=True, null=True)
    scores = models.IntegerField(blank=True, null=True, default=None)

    objects = IntraCeQueryset.as_manager()

    def __str__(self):
        return self.project.__str__()

    class Meta:
        ordering = ("-started",)

    @property
    def is_group(self):
        return self.project.is_group

    @classmethod
    def prefetch_std(cls):
        return ["user"]

    def start_or_end(self):
        return self.finished or self.started

    def get_absolute_url(self):
        return reverse(
            "project:project-register-detail", kwargs={"pk": self.project.pk}
        )

    @classmethod
    def finish(cls, user, project):
        obj = cls.objects.filter(leader=user, project=project, status=cls.__PROGRESS)
        count = obj.count()
        if count == 1:
            obj = obj.first()
            obj.finished = timezone.now()
            obj.status = cls.__FINISH
            obj.save()
            return obj
        raise ValueError(_("Vous ne pouvez pas terminer ce project"))

    @classmethod
    def close(cls, user, project):
        obj = cls.objects.filter(leader=user, project=project, status=cls.__WAIT)
        count = obj.count()
        if count == 1:
            obj = obj.first()
            if obj.teams.count() != obj.project.groups_number + 1:
                raise ValueError(_("Vous n'ete pas un group suffisant"))
            obj.finished = timezone.now()
            obj.status = cls.__PROGRESS
            obj.invitations.clear()
            obj.save()
            return obj
        raise ValueError(_("Vous ne pouvez pas clore ce project"))

    @classmethod
    def retry(cls, user, project):
        obj = (
            cls.objects.filter(leader=user, project=project)
            .filter(~Q(status=cls.__WAIT))
            .order_by("-pk")
        )
        count = obj.count()
        if count:
            obj = obj.first()
            obj.invitations.clear()
            obj.status = cls.__FINISH
            obj.save()
            new = cls(project=project, leader=user)
            new.save()
            return new
        raise ValueError(_("Vous ne pouvez pas retry ce project"))

    @classmethod
    def register(cls, user, project):
        obj = cls.objects.filter(leader=user, project=project, status=cls.__PROGRESS)
        count = obj.count()
        if not count:
            new = cls(project=project, leader=user)
            new.save()
            return new
        raise ValueError(_("Vous ne pouvez pas vous enregistrer sur ce project"))

    def is_finish(self):
        return self.status == self.__FINISH

    def is_progress(self):
        return self.status == self.__PROGRESS

    def is_waiting(self):
        return self.status == self.__WAIT

    def is_corrected(self):
        return self.status == self.__CORRECT

    def is_fail(self):
        return False if self.scores >= 70 and self.is_finish else True

    def is_success(self):
        return True if self.scores >= 70 and self.is_finish else False

    def is_notyet(self):
        return False if self.is_finish else True

    @classmethod
    def extra(cls, user, project):
        return (
            cls.objects.filter(Q(leader=user) | Q(teams=user), project=project)
            .distinct()
            .aggregate(
                retry_nb=Count("pk", distinct=True),
                scores_max=Max("scores"),
                outstanding_nb=Count(
                    "evaluation", filter=Q(evaluation__outstanding=True), distinct=True,
                ),
            )
        )
