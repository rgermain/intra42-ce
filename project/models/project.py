from django.db import models
from django.db.models import Count, Q
from django.urls import reverse
from django.utils import timezone
from common.manager import IntraCeQueryset


PROJECT_S = "project-resource/"
PROJECT_P = "project-pdf/"


class Project(models.Model):
    """
        models Projects
        one projects have many creator
    """

    name = models.CharField(max_length=50)
    description = models.TextField(null=True)
    creators = models.ManyToManyField("common.User")
    created = models.DateTimeField(default=timezone.now)
    active = models.BooleanField(default=False)
    parent = models.ForeignKey(
        "project.Project", on_delete=models.SET_NULL, null=True, blank=True,
    )
    experience = models.PositiveIntegerField(default=50)
    corrector_number = models.PositiveSmallIntegerField(default=2)
    groups_number = models.PositiveSmallIntegerField(default=1)
    resource = models.FileField(
        upload_to=PROJECT_S, max_length=100, null=True, blank=True,
    )
    documentation = models.FileField(upload_to=PROJECT_P, max_length=100)
    videos = models.ManyToManyField("project.ProjectLink", blank=True)
    languages = models.ManyToManyField("project.ProgLang", blank=True)
    category = models.ManyToManyField("project.Category", blank=True)
    objectives = models.ManyToManyField("project.Objective", blank=True)
    skills = models.ManyToManyField("project.Skill", blank=True)
    objects = IntraCeQueryset.as_manager()

    def __str__(self):
        return self.name

    @property
    def is_group(self):
        return True if self.groups_number > 1 else False

    @classmethod
    def prefetch_std(cls):
        return ["creators", "videos", "languages", "category"]

    def get_absolute_url(self):
        return reverse("project:project-detail", kwargs={"pk": self.pk})

    def get_version(self):
        # aggregate version
        if hasattr(self, "ag_version"):
            return self.ag_version
        return self.projectversion_set.aggregate(Count("pk"))["pk"]

    def extra(self):
        extra = self.__class__.objects.aggregate(
            num=Count("projectregister__pk"),
            num_finish=Count(
                "projectregister__pk", filter=Q(projectregister__status="CLOSED")
            ),
            num_student=Count("projectregister__teams__pk")
            + Count("projectregister__leader__pk"),
            num_fail=Count(
                "projectregister__pk",
                filter=Q(
                    projectregister__status="FINISH", projectregister__scores__lt=70
                ),
            ),
            num_success=Count(
                "projectregister__pk",
                filter=Q(
                    projectregister__status="FINISH", projectregister__scores__gte=70
                ),
            ),
            num_notyet=Count(
                "projectregister__pk", filter=Q(projectregister__scores__isnull=True),
            ),
        )
        return extra
