from django.contrib import admin
from .models import Project, ProjectRegister


class ProjectAdmin(admin.ModelAdmin):
    list_display = ("pk", "name", "created")


class ProjectRegisterAdmin(admin.ModelAdmin):
    list_display = ("pk", "project")


admin.site.register(Project, ProjectAdmin)
admin.site.register(ProjectRegister, ProjectRegisterAdmin)
