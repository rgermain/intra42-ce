from .models import Project, ProjectRegister
from django.forms import ModelForm
from common.models import User


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = "__all__"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({"class": "form-control"})


class ProjectRegisterForm(ModelForm):
    class Meta:
        model = ProjectRegister
        fields = ("teams", "invitations", "git")

    def __init__(self, **kwargs):
        creator = kwargs.pop("creator", None)
        super().__init__(**kwargs)
        self.fields["teams"].required = False
        self.fields["invitations"].required = False
        self.fields["git"].required = False
        self.fields["teams"].widget.attrs.update({"class": "form-control select2"})
        self.fields["invitations"].widget.attrs.update(
            {"class": "form-control select2"}
        )
        self.fields["git"].widget.attrs.update({"class": "form-control"})
        if creator:
            query = User.objects.exclude(id=creator.id)
            self.fields["teams"].queryset = query
            self.fields["invitations"].queryset = query

    def save(self, commit=True):
        if self.instance:
            self.instance.invitations.clear()
            self.instance.teams.clear()
        return super().save()
