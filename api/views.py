from rest_framework import status, viewsets, pagination
from rest_framework.response import Response
from api import serializers
from project.models import ProjectRegister


class ProjectRegisterViewSet(viewsets.ModelViewSet):
    queryset = ProjectRegister.objects.all()
    serializer_class = serializers.ProjectRegisterSerializer
