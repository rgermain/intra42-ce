from django.conf.urls import include
from rest_framework import routers
from django.urls import path
from api import views


router = routers.DefaultRouter()
router.register(
    "projectregister", views.ProjectRegisterViewSet, basename="projectregister"
)

urlpatterns = [
    path("", include((router.urls, "api"))),
]
