from rest_framework import serializers
from project.models import ProjectRegister


class ProjectRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectRegister
        fields = "__all__"

