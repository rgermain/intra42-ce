from .mixins import IntraCeMixin
from django.views.generic import DetailView
from .models import User
from django.contrib import messages
from django.utils.translation import gettext as _


class Accueil(IntraCeMixin, DetailView):
    model = User
    template_name = "accueil.html"

    def access_fnc(self):
        if not self.kwargs["pk"]:
            return True
        try:
            self.model.objects.get(id=self.kwargs["pk"])
            return True
        except self.model.DoesNotExist:
            messages.error(self.request, _("User Does Not Exist"))
        return False

    def get_object(self, query=None, *args, **kargs):
        if not self.kwargs["pk"]:
            return self.request.user
        return self.model.objects.get(id=self.kwargs["pk"])
