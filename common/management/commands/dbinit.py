from project.models import Project, ProgLang, Category, Skill, Objective
from django.core.management.base import BaseCommand
from common.models import User
from django.db.models import Q
from django.core.files import File


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            "--reset", action="store_true", help="Delete poll instead of closing it",
        )

    def handle(self, *args, **option):
        # create user
        _user = [
            {
                "username": "fchancel",
                "first_name": "florian",
                "last_name": "chancelade",
            },
            {"username": "rcepre", "first_name": "renaud", "last_name": "cepre"},
            {"username": "kranbono", "first_name": "kevin", "last_name": "kranbonoa"},
        ]
        if option["reset"]:
            User.objects.all().delete()

        User.objects.get_or_create(
            username="rgermain",
            first_name="remi",
            last_name="germain",
            is_staff=True,
            is_superuser=True,
        )
        _user_bulk = [User(**user) for user in _user]
        User.objects.bulk_create(_user_bulk)
        for user in User.objects.all():
            user.set_password("a")

        # creating lang
        proglangs = ["C", "C++", "JAVA", "JS", "PYTHON", "C#", "RUBY", "ASM"]
        bulk_lang = [ProgLang(name=name) for name in proglangs]
        if option["reset"]:
            ProgLang.objects.filter(name__in=proglangs).delete()
        ProgLang.objects.bulk_create(bulk_lang)

        # creating category
        categorys = ["ALGO", "WEB", "UNIX", "SECU", "SERVER", "REGEX"]
        bulk_cat = [Category(name=name) for name in categorys]
        if option["reset"]:
            Category.objects.filter(name__in=categorys).delete()
        Category.objects.bulk_create(bulk_cat)

        # creating skills
        skills = [
            "Security",
            "Web",
            "DB & Data ",
            "Technology integration",
            "Network & system administration",
            "Unix ",
            "Adaptation & creativity",
            "Organization",
            "Algorithms & AI",
            "Group & interpersonal",
            "Imperative programming",
            "Rigor",
            "Graphics",
            "Parallel computing",
        ]
        bulk_skill = [Skill(name=name) for name in skills]
        if option["reset"]:
            Skill.objects.filter(name__in=skills).delete()
        Skill.objects.bulk_create(bulk_skill)

        # objectives
        objectives = [
            "Compilation",
            "Machine virtuelle simpliste",
            "Langage type assembleur simpliste",
            "Rendu visuel ",
            "Algorithmes de tri",
            "Notion de pile et éléments de manipulation ",
        ]
        bulk_objective = [Objective(name=name) for name in objectives]
        if option["reset"]:
            Objective.objects.filter(name__in=skills).delete()
        Objective.objects.bulk_create(bulk_objective)

        # creating project
        import io

        fd = io.StringIO("your text goes here")

        if option["reset"]:
            name_project = ["corewar", "push-swap", "lem-in", "hypertube"]
            Project.objects.filter(name__in=name_project).delete()

        corewar = Project(
            name="corewar",
            description="bla bla bla description",
            experience=500,
            corrector_number=5,
            groups_number=1,
            documentation=File(fd, name="file.pdf"),
        )
        corewar.save()
        corewar.languages.add(ProgLang.objects.get(name="C"))
        corewar.languages.add(ProgLang.objects.get(name="ASM"))
        corewar.category.add(Category.objects.get(name="ALGO"))

        pushswap = Project(
            name="push-swap",
            description="bla bla bla description",
            experience=500,
            corrector_number=5,
            groups_number=1,
            documentation=File(fd, name="file.pdf"),
        )
        pushswap.save()
        pushswap.languages.add(ProgLang.objects.get(name="C"))
        pushswap.category.add(Category.objects.get(name="ALGO"))

        lemin = Project(
            name="lem-in",
            description="bla bla bla description",
            experience=500,
            corrector_number=5,
            groups_number=1,
            documentation=File(fd, name="file.pdf"),
        )
        lemin.save()
        lemin.languages.add(ProgLang.objects.get(name="C"))
        lemin.category.add(Category.objects.get(name="ALGO"))

        hypertune = Project(
            name="hypertube",
            description="bla bla bla description",
            experience=500,
            corrector_number=5,
            groups_number=1,
            documentation=File(fd, name="file.pdf"),
        )
        hypertune.save()
        hypertune.category.add(Category.objects.get(name="WEB"))
        hypertune.skills.add(Skill.objects.get(name="Security"))
        hypertune.skills.add(Skill.objects.get(name="Web"))
        hypertune.skills.add(Skill.objects.get(name="DB & Data "))
        hypertune.skills.add(Skill.objects.get(name="Network & system administration"))

