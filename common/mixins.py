from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.shortcuts import redirect

# LoginRequiredMixin
class IntraCeMixin(UserPassesTestMixin):
    """
        class based django mixin for redirect and check access to view
    """

    redirect_no_permission = None
    request = None

    def access_fnc(self):
        return True

    def get_test_func(self):
        return self.access_fnc

    def __redirect_referer_or_link(self, request, link=None, **kwargs):
        if (
            "HTTP_REFERER" in request.META.keys()
            and request.META["HTTP_REFERER"] != request.build_absolute_uri()
        ):
            return redirect(request.META["HTTP_REFERER"])
        # create redirect name
        current = (
            request.resolver_match.namespaces[0] + ":" + request.resolver_match.url_name
        )
        if link and link != current:
            return redirect(link, **kwargs)
        return redirect("accueil")

    def handle_no_permission(self):
        if self.request and self.request.user.is_authenticated:
            return self.__redirect_referer_or_link(
                self.request, self.redirect_no_permission
            )
        return super().handle_no_permission()
