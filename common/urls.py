from django.urls import path
from .views import Accueil


app_name = "common"

urlpatterns = [
    path("accueil/", Accueil.as_view(), {"pk": None}, name="accueil"),
    path("user/<int:pk>/", Accueil.as_view(), name="user-detail"),
    path("", Accueil.as_view(), {"pk": None}),
]
