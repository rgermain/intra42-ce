from django.db import models


class IntraCeQueryset(models.QuerySet):
    def prefetch_std(self, prefetch=None, select=None):
        """
            prefetch with method in class
        """
        query = self
        prefetch_lst = prefetch if prefetch else list()
        select_lst = select if select else list()
        if hasattr(self.model, "prefetch_std"):
            prefetch_lst += self.model.prefetch_std()
        if hasattr(self.model, "select_std"):
            select_lst += self.model.select_std()
        if prefetch_lst:
            query = query.prefetch_related(*prefetch_lst)
        if select_lst:
            query = query.select_related(*select_lst)
        return query
