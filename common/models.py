from django.db import models
from django.db.models import Count, Q, Avg
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext as _
from .manager import IntraCeQueryset
from evaluation.models import Evaluation
from project.models import Project, ProjectRegister


class User(AbstractUser):
    correction_points = models.IntegerField(_("correction points"), default=5)
    experiences = models.IntegerField(_("experiences"), default=0)

    # objects = IntraCeQueryset.as_manager()

    class Meta(AbstractUser.Meta):
        swappable = "AUTH_USER_MODEL"

    def location(self):
        # check where is connect
        return "Unavailable"

    def grade(self):
        grades = (
            (500, _("padawan")),
            (1000, _("maitre")),
        )
        for grade in grades:
            if self.experiences < grade[0]:
                return grade[1]
        return "The Duc"

    def statistic(self):
        stat = {
            _("Nombre de outstaning"): Evaluation.objects.filter(outstanding=True)
            .filter(Q(project__leader=self) | Q(project__teams=self))
            .distinct()
            .count(),
            _("Nombre de project"): ProjectRegister.objects.filter(
                Q(leader=self) | Q(teams=self)
            )
            .distinct()
            .count(),
            _("Nombre d'evaluation"): Evaluation.objects.filter(evaluator=self).count(),
            _("Score moyen"): Evaluation.objects.filter(
                Q(project__leader=self) | Q(project__teams=self)
            ).aggregate(average=Avg("final_score", distinct=True),)["average"]
            or 0,
        }
        return stat
