from django import template

register = template.Library()


@register.filter
def tc_photo(photo):
    return photo if photo else "/static/img/user.png"
