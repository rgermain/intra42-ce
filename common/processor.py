from .models import User
from project.models import Project
from evaluation.models import Evaluation
from project.models import ProjectRegister
from django.utils import timezone
from django.db.models import Q
from utility.fnc_utils import generate_notifications


def user_info(request):
    user = User.objects.first()
    date = user.last_login or timezone.now()
    info = {
        "user": user,
        "has_new_project": True
        if Project.objects.filter(created__lt=date).count()
        else False,
        "evaluations": Evaluation.objects.filter(
            Q(project__leader=user) | Q(project__teams=user), is_validated=False
        ),
        "currentproject": ProjectRegister.objects.filter(
            Q(leader=user) | Q(teams=user), finished__isnull=True
        ).distinct(),
        "allproject": ProjectRegister.objects.all().order_by("finished"),
        "notifications": generate_notifications(
            ProjectRegister.objects.filter(invitations=user)
        ),
    }
    return info


def git_info(request):
    info = {"git": {"version": "Alpha 1.0"}}
    return info
