"""_main_ URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib.staticfiles.urls import static

from django.contrib import admin
from django.urls import path, include, re_path
from _main_ import settings

urlpatterns = [
    path("admin/", admin.site.urls),
    path("project/", include("project.urls", namespace="project")),
    path("api-auth/", include("rest_framework.urls")),
    path("api/", include("api.urls"), name="api"),
    path("", include("common.urls", namespace="common")),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

    if "debug_toolbar" in settings.INSTALLED_APPS:
        urlpatterns += [
            re_path(r"^__debug__/", include(debug_toolbar.urls)),
        ]
